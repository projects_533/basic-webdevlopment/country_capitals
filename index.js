import express from "express";
import bodyParser from "body-parser";
import pg  from "pg";

const app=express();
const port=3000;


let quiz=[];
let totalscore=0;
let currentCountry={}; 


const db=new pg.Client({
    user:"postgres",
    host:"localhost",
    database:"My_database",
    password:"Suraj@123",
    port:5432,
});

db.connect();

db.query("select * from capitals",(err,res)=>{
    if(err){
        console.log("some error occured ",err.stack);
    }else{
        quiz=res.rows;
    }
    db.end();
});




app.use(bodyParser.urlencoded({extended:true}));
app.use(express.static("public"));




app.get("/",async(req,res)=>{
    totalscore=0;
    await nextQuestion();
    res.render("index.ejs",{question:currentCountry.country});
});



app.post('/submit',(req,res)=>{
    let answer=req.body.answer.trim();//inorder  to get rid of the spaces...
    let isCorrect=false;
    let curr_countryCapital='';
    
    try{
         curr_countryCapital=currentCountry.capital.toLowerCase().trim();
    }catch(err){
        curr_countryCapital='null';
    }
   
    if(answer.toLowerCase()===curr_countryCapital){
        totalscore++;   
        isCorrect=true;
    }

    nextQuestion();
    res.render("index.ejs",{
        question:currentCountry.country,
        wasCorrect:isCorrect,
        totalScore:totalscore,
    });



});




const nextQuestion=()=>{
    let idx=Math.round(Math.random()*quiz.length)
    let randomcountry=quiz[idx]
    currentCountry=randomcountry;
    console.log(currentCountry);
}


app.listen(port,()=>{
    console.log("\n\nRunning on the port 3000");
});